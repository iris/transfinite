
From iris.program_logic.refinement Require Export ref_weakestpre ref_adequacy seq_weakestpre.
From iris.examples.refinements Require Export refinement.
From iris.algebra Require Import auth.
From iris.heap_lang Require Import proofmode notation.
From iris.proofmode Require Import tactics.
Set Default Proof Using "Type".


(* We illustrate here how to derive the rules shown in the paper *)


Section derived.
  Context {SI: indexT} `{Σ: gFunctors SI} `{!rheapG Σ} `{!auth_sourceG Σ (natA SI)} `{!seqG Σ}.

  Definition seq_rswp E e φ : iProp Σ := (na_own seqG_name E -∗ RSWP e at 0 ⟨⟨ v, na_own seqG_name E ∗ φ v ⟩⟩)%I.
  Notation "⟨⟨ P ⟩ ⟩ e ⟨⟨ v , Q ⟩ ⟩" := (□ (P -∗ (seq_rswp ⊤ e (λ v, Q))))%I
  (at level 20, P, e, Q at level 200, format "⟨⟨  P  ⟩ ⟩  e  ⟨⟨  v ,  Q  ⟩ ⟩") : stdpp_scope.
  Notation "{{ P } } e {{ v , Q } }" := (□ (P -∗ SEQ e ⟨⟨ v, Q ⟩⟩))%I
  (at level 20, P, e, Q at level 200, format "{{  P  } }  e  {{  v ,  Q  } }") : stdpp_scope.
  Notation "'|==>src' P" := (weak_src_update ⊤ P)%I (at level 99) : stdpp_scope.


  Lemma Conseq (e: expr) P P' Q Q':
    (P ⊢ P') →
    (∀ v, Q' v ⊢ Q v) →
    ({{P'}} e {{v, Q' v}} ⊢ {{P}} e {{v, Q v}})%I.
  Proof.
    iIntros (He1 He2) "#Hh". iModIntro. rewrite He1.
    iIntros "HP Hna". iApply (rwp_wand with "(Hh HP Hna)").
    iIntros (v) "[$ HQ]". rewrite He2 //.
  Qed.

  Lemma HoareExists {X} (e: expr) P Q :
    (∀ x, {{P x}} e {{v, Q v}})  ⊢ {{ ∃ x: X, P x}} e {{v, Q v}}%I.
  Proof.
    iIntros "#H !>". iDestruct 1 as (x) "HP". iApply ("H" with "HP").
  Qed.

  Lemma HoarePure (e: expr) φ P Q :
    (P ⊢ ⌜φ⌝) →  (⌜φ⌝ -∗ {{P}} e {{v, Q v}}) ⊢ {{P}} e {{v, Q v}}%I.
  Proof.
    iIntros (Hent) "#H !> HP". iDestruct (Hent with "HP") as %Hent'.
    iApply ("H" with "[//] HP").
  Qed.

  Lemma Value (v: val): (⊢ {{True}} v {{w, ⌜v = w⌝}})%I.
  Proof.
    iIntros "!> _ $". by iApply rwp_value.
  Qed.

  Lemma Frame (e: expr) P R Q: ({{P}} e {{v, Q v}} ⊢ {{P ∗ R}} e {{v, Q v ∗ R}})%I.
  Proof.
    iIntros "#H !> [P $]". by iApply "H".
  Qed.

  Lemma Bind (e: expr) K P Q R:
    ({{P}} e {{v, Q v}} ∗ (∀ v: val, ({{Q v}} fill K (Val v) {{w, R w}}))
    ⊢ {{P}} fill K e {{v, R v}})%I.
  Proof.
    iIntros "[#H1 #H2] !> P Hna".
    iApply rwp_bind. iSpecialize ("H1" with "P Hna").
    iApply (rwp_strong_mono with "H1 []"); auto.
    iIntros (v) "[Hna Q] !>". iApply ("H2" with "Q Hna").
  Qed.

  Lemma Löb (P : iPropI Σ) : (▷ P → P) ⊢ P.
  Proof. iApply bi.löb. Qed.

  Lemma TPPureT (e e': expr) P Q: pure_step e e' → ({{P}} e' {{v, Q v}} ⊢ ⟨⟨P⟩⟩ e ⟨⟨v, Q v⟩⟩)%I.
  Proof.
    iIntros (Hstep) "#H !> P Hna".
    iApply (ref_lifting.rswp_pure_step_later _ _ _ _ _ True); [|done|by iApply ("H" with "P Hna")].
    intros _. apply nsteps_once, Hstep.
  Qed.

  Lemma TPPureS (e e' et: expr) K P Q:
    to_val et = None
    → pure_step e e'
    → (⟨⟨src (fill K e') ∗ P⟩⟩ et ⟨⟨v, Q v⟩⟩ ⊢ {{src (fill K e) ∗ ▷ P}} et {{v, Q v}})%I.
  Proof.
    iIntros (Hexp Hstep) "#H !> [Hsrc P] Hna". iApply (rwp_take_step with "[P Hna] [Hsrc]"); first done; last first.
    - iApply step_pure; last iApply "Hsrc". apply pure_step_ctx; last done. apply _.
    - iIntros "Hsrc'". iApply rswp_do_step. iNext. iApply ("H" with "[$P $Hsrc'] Hna").
  Qed.

  Lemma TPStoreT l (v1 v2: val): (True ⊢ ⟨⟨l ↦ v1⟩⟩ #l <- v2 ⟨⟨w, ⌜w = #()⌝ ∗ l ↦ v2⟩⟩)%I.
  Proof.
    iIntros "_ !> Hl $". iApply (rswp_store with "[$Hl]").
    by iIntros "$".
  Qed.

  Lemma TPStoreS (et: expr) l v1 v2 K P Q:
    to_val et = None
    → (⟨⟨P ∗ src (fill K (Val #())) ∗ l ↦s v2⟩⟩ et ⟨⟨v, Q v⟩⟩
      ⊢ {{src (fill K (#l <- v2)) ∗ l ↦s v1 ∗ ▷ P}} et {{v, Q v}})%I.
  Proof.
    iIntros (Hexp) "#H !> [Hsrc [Hloc P]] Hna". iApply (rwp_take_step with "[P Hna] [Hsrc Hloc]"); first done; last first.
    - iApply step_store. iFrame.
    - iIntros "Hsrc'". iApply rswp_do_step. iNext. iApply ("H" with "[$P $Hsrc'] Hna").
  Qed.

  Lemma TPStutterT (e: expr) P Q: to_val e = None → (⟨⟨P⟩⟩ e ⟨⟨v, Q v⟩⟩ ⊢ {{P}} e {{v, Q v}})%I.
  Proof.
    iIntros (H) "#H !> P Hna". iApply rwp_no_step; first done.
    by iApply ("H" with "P Hna").
  Qed.

  Lemma TPStutterSStore (et : expr) v1 v2 K l P Q :
    to_val et = None
    → {{P ∗ src (fill K (Val #())) ∗ l ↦s v2}} et {{v, Q v}}
    ⊢ {{l ↦s v1 ∗ src (fill K (#l <- v2)) ∗ P}} et {{v, Q v}}.
  Proof.
    iIntros (Hv) "#H !> [Hloc [Hsrc P]] Hna".
    iApply (rwp_weaken with "[H Hna] [P Hloc Hsrc]"); first done.
    - instantiate (1 := (P ∗ src (fill K #()) ∗ l ↦s v2)%I).
      iIntros "H1". iApply ("H" with "[H1] [Hna]"); done.
    - iApply src_update_mono. iSplitL "Hsrc Hloc".
      iApply step_store. by iFrame.
      iIntros "[H0 H1]". by iFrame.
  Qed.

  Lemma TPStutterSPure (et es es' : expr) P Q :
    to_val et = None
    → pure_step es es'
    → {{ P ∗ src(es') }} et {{v, Q v}}
      ⊢ {{ P ∗ src(es)}} et {{v, Q v}}.
  Proof.
    iIntros (H0 H) "#H !> [P Hsrc] Hna".
    iApply (rwp_weaken with "[H Hna] [P Hsrc]"); first done.
    - instantiate (1 := (P ∗ src es')%I). iIntros "H1".
      by iApply ("H" with "[H1] Hna").
    - iApply src_update_mono. iSplitL "Hsrc".
      by iApply step_pure. iIntros "?"; by iFrame.
  Qed.

  Lemma HoareLöb X P Q e :
    (∀ x : X, {{P x ∗ ▷ (∀ x, {{P x}} e x {{v, Q x v}})}} e x {{ v, Q x v}})
    ⊢ ∀ x, {{ P x }} e x {{v, Q x v}}.
  Proof.
    iIntros "#H". iApply bi.löb.
    iIntros "#H1" (x).
    iModIntro. iIntros "HP". iApply "H".
    iFrame "#". iFrame.
  Qed.

  Lemma HoareLöbNoArgs P Q e :
    ({{P ∗ ▷ ({{P}} e {{v, Q v}})}} e {{ v, Q v}})
    ⊢ {{ P }} e {{v, Q v}}.
  Proof.
    iIntros "#H". iApply bi.löb.
    iIntros "#H1".
    iModIntro. iIntros "HP". iApply "H".
    iFrame "#". iFrame.
  Qed.

  (* Extended Refinement Program Logic of Transfinite Iris *)
  Lemma value_tgt_tpr (v: val): (⊢ {{True}} v {{w, ⌜v = w⌝}})%I.
  Proof.
    iIntros "!> _ $". by iApply rwp_value.
  Qed.


  Lemma bind_tgt_tpr (e: expr) K P Q R:
    ({{P}} e {{v, Q v}} ∗ (∀ v: val, ({{Q v}} fill K (Val v) {{w, R w}}))
    ⊢ {{P}} fill K e {{v, R v}})%I.
  Proof.
    iIntros "[#H1 #H2] !> P Hna".
    iApply rwp_bind. iSpecialize ("H1" with "P Hna").
    iApply (rwp_strong_mono with "H1 []"); auto.
    iIntros (v) "[Hna Q] !>". iApply ("H2" with "Q Hna").
  Qed.

  Lemma pure_tgt_tpr (e e': expr) P Q: pure_step e e' → ({{P}} e' {{v, Q v}} ⊢ ⟨⟨P⟩⟩ e ⟨⟨v, Q v⟩⟩)%I.
  Proof.
    iIntros (Hstep) "#H !> P Hna".
    iApply (ref_lifting.rswp_pure_step_later _ _ _ _ _ True); [|done|by iApply ("H" with "P Hna")].
    intros _. apply nsteps_once, Hstep.
  Qed.


  Lemma pure_src_tpr (e e' et: expr) K P Q:
    to_val et = None
    → pure_step e e'
    → (⟨⟨src (fill K e') ∗ P⟩⟩ et ⟨⟨v, Q v⟩⟩ ⊢ {{src (fill K e) ∗ ▷ P}} et {{v, Q v}})%I.
  Proof.
    iIntros (Hexp Hstep) "#H !> [Hsrc P] Hna". iApply (rwp_take_step with "[P Hna] [Hsrc]"); first done; last first.
    - iApply step_pure; last iApply "Hsrc". apply pure_step_ctx; last done. apply _.
    - iIntros "Hsrc'". iApply rswp_do_step. iNext. iApply ("H" with "[$P $Hsrc'] Hna").
  Qed.


  Lemma store_tgt_tpr l (v1 v2: val): (True ⊢ ⟨⟨l ↦ v1⟩⟩ #l <- v2 ⟨⟨w, ⌜w = #()⌝ ∗ l ↦ v2⟩⟩)%I.
  Proof.
    iIntros "_ !> Hl $". iApply (rswp_store with "[$Hl]").
    by iIntros "$".
  Qed.

  Lemma store_src_tpr (et: expr) l v1 v2 K P Q:
    to_val et = None
    → (⟨⟨src (fill K (Val #())) ∗ l ↦s v2 ∗ P⟩⟩ et ⟨⟨v, Q v⟩⟩
      ⊢ {{src (fill K (#l <- v2)) ∗ l ↦s v1 ∗ ▷ P}} et {{v, Q v}})%I.
  Proof.
    iIntros (Hexp) "#H !> [Hsrc [Hloc P]] Hna". iApply (rwp_take_step with "[P Hna] [Hsrc Hloc]"); first done; last first.
    - iApply step_store. iFrame.
    - iIntros "Hsrc'". iApply rswp_do_step. iNext. iApply ("H" with "[$P $Hsrc'] Hna").
  Qed.

  Lemma load_tgt_tpr l v: (True ⊢ ⟨⟨l ↦ v⟩⟩ ! #l ⟨⟨w, ⌜w = v⌝ ∗ l ↦ v⟩⟩)%I.
  Proof.
    iIntros "_ !> Hl $". iApply (rswp_load with "[$Hl]").
    by iIntros "$".
  Qed.

  Lemma load_src_tpr (et: expr) l v K P Q:
    to_val et = None
    → (⟨⟨src (fill K (Val v)) ∗ l ↦s v ∗ P⟩⟩ et ⟨⟨w, Q w⟩⟩
      ⊢ {{src (fill K (! #l)) ∗ l ↦s v ∗ ▷ P}} et {{w, Q w}})%I.
  Proof.
    iIntros (Hexp) "#H !> [Hsrc [Hloc P]] Hna". iApply (rwp_take_step with "[P Hna] [Hsrc Hloc]"); first done; last first.
    - iApply step_load. iFrame.
    - iIntros "Hsrc'". iApply rswp_do_step. iNext. iApply ("H" with "[$P $Hsrc'] Hna").
  Qed.

  Lemma ref_tgt_tpr (v: val): (True ⊢ ⟨⟨ True ⟩⟩ ref v ⟨⟨w, ∃ l: loc, ⌜w = #l⌝ ∗ l ↦ v⟩⟩)%I.
  Proof.
    iIntros "_ !> _ $". iApply (rswp_alloc with "[//]").
    iIntros (l) "[Hl _]". iExists l. by iFrame.
  Qed.

  Lemma ref_src_tpr (et: expr) v K P Q:
    to_val et = None
    → (⟨⟨∃ l: loc, src (fill K (Val #l)) ∗ l ↦s v ∗ P⟩⟩ et ⟨⟨w, Q w⟩⟩
      ⊢ {{src (fill K (ref v)) ∗ ▷ P}} et {{w, Q w}})%I.
  Proof.
    iIntros (Hexp) "#H !> [Hsrc HP] Hna". iApply (rwp_take_step with "[HP Hna] [Hsrc]"); first done; last first.
    - iApply step_alloc. iFrame.
    - iIntros "Hsrc'". iApply rswp_do_step. iNext. iApply ("H" with "[$HP $Hsrc'] Hna").
  Qed.


  Lemma stutter_src_tpr (e: expr) P Q: to_val e = None → (⟨⟨P⟩⟩ e ⟨⟨v, Q v⟩⟩ ⊢ {{P}} e {{v, Q v}})%I.
  Proof.
    iIntros (H) "#H !> P Hna". iApply rwp_no_step; first done.
    by iApply ("H" with "P Hna").
  Qed.

  Lemma stutter_tgt_tpr (et: expr) P Q :
    to_val et = None →
    {{P}} et {{v, Q v}} ⊢ {{ |==>src P }} et {{v, Q v}}.
  Proof.
    iIntros (H) "#H !> P Hna".
    iApply (rwp_weaken' P with "[Hna]"); first done.
    - iIntros "HP". by wp_apply ("H" with "HP").
    - done.
  Qed.

  (* implicit stuttering *)
  Lemma src_upd_intro P : P ⊢ |==>src P.
  Proof.
    iIntros "P". by iApply weak_src_update_return.
  Qed.

  Lemma src_upd_bind P Q : (|==>src P) ∗ (P -∗ |==>src Q) ⊢ |==>src Q.
  Proof. eapply weak_src_update_bind. Qed.

  Lemma src_upd_frame P Q : (|==>src P) ∗ Q ⊢ |==>src (P ∗ Q).
  Proof.
    iIntros "[HP HQ]". iApply (src_upd_bind with "[$HP HQ]").
    iIntros "HP". iApply src_upd_intro. iFrame.
  Qed.

  Lemma src_upd_pure e e' K :
    pure_step e e' →
    ((src (fill K e)) ⊢ |==>src (src (fill K e'))).
  Proof.
    iIntros (Hstep) "Hsrc". iApply (src_update_weak_src_update).
    iApply (step_pure with "Hsrc").
    apply pure_step_ctx; last done. apply _.
  Qed.

  Lemma src_upd_store (l: loc) (v w: val) K :
    src (fill K (#l <- w)) ∗ (l ↦s v) ⊢ |==>src src (fill K #()) ∗ (l ↦s w).
  Proof.
    iIntros "[Hsrc Hl]". iApply src_update_weak_src_update.
    iApply step_store. iFrame.
  Qed.

  Lemma src_upd_load (l: loc) (v: val) K :
    src (fill K (! #l)) ∗ (l ↦s v) ⊢ |==>src src (fill K v) ∗ (l ↦s v).
  Proof.
    iIntros "[Hsrc Hl]". iApply src_update_weak_src_update.
    iApply step_load. iFrame.
  Qed.

  Lemma src_upd_alloc (v: val) K :
    src (fill K (ref v)) ⊢ |==>src ∃ (l: loc), src (fill K #l) ∗ l ↦s v.
  Proof.
    iIntros "Hsrc". iApply src_update_weak_src_update.
    iApply step_alloc. iFrame.
  Qed.

  (* explicit stuttering *)
  Lemma src_stutter_cred P et Q :
    to_val et = None →
    ⟨⟨ P ⟩⟩ et ⟨⟨ v, Q v ⟩⟩ ⊢  {{ $ 1 ∗ ▷ P}} et {{v, Q v}}.
  Proof.
    iIntros (H) "#H !> [Hc P]  Hna". iApply (rwp_take_step with "[Hna P] [Hc]"); first done; last first.
    - iApply step_stutter. iFrame.
    - iIntros "_". iApply rswp_do_step. iNext.
      iApply ("H" with "[$P] Hna").
  Qed.

  Lemma src_stutter_cred_split n m :
    $ (n + m) ⊣⊢ $ n ∗ $ m.
  Proof.
    rewrite srcF_split //.
  Qed.

  Lemma pure_src_stutter_tpr (e e' et: expr) (n: nat) K P Q:
    to_val et = None
    → pure_step e e'
    → (⟨⟨src (fill K e') ∗ $ n ∗ P⟩⟩ et ⟨⟨v, Q v⟩⟩ ⊢ {{src (fill K e) ∗ ▷ P}} et {{v, Q v}})%I.
  Proof.
    iIntros (Hexp Hstep) "#H !> [Hsrc P] Hna". iApply (rwp_take_step with "[P Hna] [Hsrc]"); first done; last first.
    - iApply (step_pure_cred with "Hsrc").
      apply pure_step_ctx; last done. apply _.
    - iIntros "Hsrc'". iApply rswp_do_step. iNext. iApply ("H" with "[$P $Hsrc'] Hna").
  Qed.

  Lemma store_src_stutter_tpr (et: expr) l v1 v2 n K P Q:
    to_val et = None
    → (⟨⟨src (fill K (Val #())) ∗ l ↦s v2 ∗ $ n ∗ P⟩⟩ et ⟨⟨v, Q v⟩⟩
      ⊢ {{src (fill K (#l <- v2)) ∗ l ↦s v1 ∗ ▷ P}} et {{v, Q v}})%I.
  Proof.
    iIntros (Hexp) "#H !> [Hsrc [Hloc P]] Hna". iApply (rwp_take_step with "[P Hna] [Hsrc Hloc]"); first done; last first.
    - iApply (step_inv_alloc with "[Hloc] Hsrc").
      iSplitL. { iIntros "Hl". iApply step_store. iFrame. }
      iIntros "[Hsrc Hl]". iExists _. iFrame.
      iPureIntro. intros Heq%(inj (fill K)). simplify_eq.
    - iIntros "[[Hsrc' Hl] Hc]". iApply rswp_do_step. iNext.
      iApply ("H" with "[$P $Hsrc' $Hc $Hl] Hna").
  Qed.

  Lemma load_src_stutter_tpr (et: expr) l v n K P Q:
    to_val et = None
    → (⟨⟨src (fill K (Val v)) ∗ l ↦s v ∗ $ n ∗ P⟩⟩ et ⟨⟨w, Q w⟩⟩
      ⊢ {{src (fill K (! #l)) ∗ l ↦s v ∗ ▷ P}} et {{w, Q w}})%I.
  Proof.
    iIntros (Hexp) "#H !> [Hsrc [Hloc P]] Hna". iApply (rwp_take_step with "[P Hna] [Hsrc Hloc]"); first done; last first.
    - iApply (step_inv_alloc with "[Hloc] Hsrc").
      iSplitL. { iIntros "Hl". iApply step_load. iFrame. }
      iIntros "[Hsrc Hl]". iExists _. iFrame.
      iPureIntro. intros Heq%(inj (fill K)). simplify_eq.
    - iIntros "[[Hsrc' Hl] Hc]". iApply rswp_do_step. iNext.
      iApply ("H" with "[$P $Hsrc' $Hc $Hl] Hna").
  Qed.

  Lemma ref_src_stutter_tpr (et: expr) v n K P Q:
    to_val et = None
    → (⟨⟨∃ l: loc, src (fill K (Val #l)) ∗ l ↦s v ∗ $ n ∗ P⟩⟩ et ⟨⟨w, Q w⟩⟩
      ⊢ {{src (fill K (ref v)) ∗ ▷ P}} et {{w, Q w}})%I.
  Proof.
    iIntros (Hexp) "#H !> [Hsrc HP] Hna". iApply (rwp_take_step with "[HP Hna] [Hsrc]"); first done; last first.
    - iApply (step_inv_alloc with "[] Hsrc").
      iSplitL. { iIntros "Hl". iApply step_alloc. iFrame. }
      iDestruct 1 as (l) "[Hsrc Hl]". iExists _. iFrame.
      iPureIntro. intros Heq%(inj (fill K)). simplify_eq.
    - iIntros "[Hsrc Hc]". iDestruct "Hsrc" as (l) "[Hsrc Hl]".
      iApply rswp_do_step. iNext.
      iApply ("H" with "[HP Hl Hsrc Hc] Hna").
      iExists _. iFrame.
  Qed.

End derived.
