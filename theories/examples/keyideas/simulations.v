From iris.base_logic Require Export iprop satisfiable.
From iris.bi Require Export fixpoint.
From iris.proofmode Require Import tactics.


Section simulations.

  Context {SI} `{LargeIndex SI} {Σ: gFunctors SI}.


  (* We assume a source and a target language *)
  Variable (S T: Type) (src_step: S → S → Prop) (tgt_step: T → T → Prop).
  Variable (V: Type) (val_to_src: V → S) (val_to_tgt: V → T).
  Variable (val_irred: ∀ v, ¬ ∃ t', tgt_step (val_to_tgt v) t') (val_inj: Inj eq eq val_to_tgt).


  (* refinements *)
  Definition rpr (t: T) (s: S) :=
    (∀ v, rtc tgt_step t (val_to_tgt v) → rtc src_step s (val_to_src v)).

  Definition tpr (t: T) (s: S) :=
    (∀ v, rtc tgt_step t (val_to_tgt v) → rtc src_step s (val_to_src v)) ∧
    (ex_loop tgt_step t → ex_loop src_step s).

  Definition sim_pre (sim: (T -d> S -d> iProp Σ)) : T -d> S -d> iProp Σ :=
    (λ t s,
      (∃ v, ⌜val_to_src v = s⌝ ∧ ⌜val_to_tgt v = t⌝) ∨
      (∃ t', ⌜tgt_step t t'⌝) ∧
      (∀ t', ⌜tgt_step t t'⌝ → ∃ s', ⌜src_step s s'⌝ ∧ ▷ sim t' s')
    )%I.

  Instance sim_pre_contr: Contractive sim_pre.
  Proof.
    intros a sim sim' Heq. unfold sim_pre.
    intros t s. do 8 f_equiv. apply bi.later_contractive.
    intros ??. by apply Heq.
  Qed.


  Definition sim := fixpoint sim_pre.

  Lemma sim_unfold':
    sim ≡ sim_pre sim.
  Proof. by rewrite {1}/sim fixpoint_unfold. Qed.

  Lemma sim_unfold t s:
    (sim t s ⊣⊢ ((∃ v, ⌜val_to_src v = s⌝ ∧ ⌜val_to_tgt v = t⌝) ∨
    (∃ t', ⌜tgt_step t t'⌝) ∧
    (∀ t', ⌜tgt_step t t'⌝ → ∃ s', ⌜src_step s s'⌝ ∧ ▷ sim t' s')))%I.
  Proof. apply sim_unfold'. Qed.

  Instance sim_plain t s: Plain (sim t s).
  Proof.
    unfold Plain. iRevert (t s). iLöb as "IH".
    iIntros (t s); rewrite sim_unfold.
    iIntros "[H1|[H1 H2]]".
    - iLeft. iApply (plain with "H1").
    - iRight. iSplit; first iApply (plain with "H1").
      iIntros (t' Hstep). iDestruct ("H2" $! t' Hstep) as (s' Hstep') "Hsim".
      iExists s'; iSplit; first (iApply plain; by iPureIntro).
      iApply later_plainly_1. iNext. by iApply "IH".
  Qed.

  Lemma sim_valid_satisfiable t s: satisfiable (sim t s) ↔ ⊢ sim t s.
  Proof.
    split.
    - intros ? % satisfiable_elim; eauto. apply _.
    - by intros ? % satisfiable_intro.
  Qed.


  Lemma satisfiable_pure φ: satisfiable (⌜φ⌝: iProp Σ)%I → φ.
  Proof.
    intros Hsat. apply satisfiable_elim in Hsat; last apply _.
    by apply uPred.pure_soundness in Hsat.
  Qed.

  (* result preserving *)
  Lemma sim_execute_tgt_step t s t':
    tgt_step t t' → satisfiable (sim t s) → ∃ s', src_step s s' ∧ satisfiable (sim t' s').
  Proof.
    intros Hstep Hsat.
    eapply satisfiable_mono with (Q := (∃ s', ⌜src_step s s'⌝ ∧ ▷ sim t' s')%I) in Hsat.
    - eapply satisfiable_exists in Hsat as [s' Hsat].
      exists s'. split.
      + eapply satisfiable_pure, satisfiable_mono; eauto. iIntros "[$ _]".
      + eapply satisfiable_later, satisfiable_mono; eauto. iIntros "[_ $]".
    - iIntros "Hsim". rewrite sim_unfold. iDestruct "Hsim" as "[Hsim|[_ Hsim]]".
     + iDestruct "Hsim" as (v) "[<- <-]". exfalso. naive_solver.
     + iApply ("Hsim" $! t' Hstep).
   Qed.

  Lemma sim_execute_tgt t s t':
    rtc tgt_step t t' → satisfiable (sim t s)
    → ∃ s', rtc src_step s s' ∧ satisfiable (sim t' s').
  Proof.
    induction 1 in s.
    - intros Hsim. exists s. by split.
    - intros Hsim. eapply sim_execute_tgt_step in Hsim; eauto.
      destruct Hsim as [s' [Hsrc Hsat]].
      destruct (IHrtc _ Hsat) as [s'' [Hsrc' Hsat']].
      exists s''. split; auto. by eapply rtc_l.
  Qed.

  (* Lemma 2.1 *)
  Lemma sim_is_rpr t s: (⊢ sim t s) → rpr t s.
  Proof.
    intros Hsim % sim_valid_satisfiable v Hsteps.
    eapply sim_execute_tgt in Hsteps as [s' [Hsteps' Hsat]]; eauto.
    enough (s' = (val_to_src v)) as -> by eauto.
    eapply satisfiable_pure, satisfiable_mono; eauto.
    rewrite sim_unfold. iIntros "[H|[H _]]".
    - iDestruct "H" as (v')  "[<- H]". by iDestruct "H" as %->%val_inj.
    - iDestruct "H" as (t') "%". exfalso. naive_solver.
  Qed.


  (* Lemma 2.2 *)
  Lemma sim_is_tpr t s: (⊢ sim t s) → tpr t s.
  Proof.
    intros Hsim. split.
    - by apply sim_is_rpr.
    - apply sim_valid_satisfiable in Hsim. revert t s Hsim.
      cofix IH. intros t s Hsat. inversion 1 as [t'' t' Hstep Hloop]; subst t''.
      destruct (sim_execute_tgt_step _ _ _ Hstep Hsat) as [s' [Hstep' Hsat']].
      econstructor; eauto.
  Qed.
End simulations.



From Coq.Logic Require Import Classical_Prop.

Section finite_stepindex_simulation.
  Context {SI} {Σ: gFunctors SI} `{!FiniteIndex SI}.

  (* Some generic properties about reduction relations *)

  Lemma ex_loop_sn {X: Type} (R: relation X) (x: X):
    ex_loop R x ↔ ¬ sn R x.
  Proof.
    split.
    - intros Hloop Hsn. by eapply acc_not_ex_loop.
    - intros Hsn. revert x Hsn. cofix IH.
      intros x Hsn. enough (∃ x', R x x' ∧ ¬ sn R x') as [x' [Hstep Hsn']].
      { econstructor; eauto. }
      destruct (classic (∃ x' : X, R x x' ∧ ¬ sn R x')); first done.
      exfalso. eapply Hsn. constructor. intros x' Hstep.
      destruct (classic (sn R x')); first done.
      exfalso. eapply H. eauto.
  Qed.

  Lemma sn_finite_nondet_bounded {X: Type} (R: relation X) (x: X):
    (∀ x, ∃ X: list X, (∀ x', R x x' ↔ x' ∈ X)) →
    sn R x →
    ∃ n, ∀ x' m, nsteps R m x x' → m < n.
  Proof.
    intros Hbounded. induction 1.
    destruct (Hbounded x) as [xs Hbounded'].
    assert (∀ y, y ∈ xs → ∃ n : nat, ∀ (x' : X) (m : nat), nsteps R m y x' → m < n) as Hx.
    { intros ??. eapply H0. simpl. eapply Hbounded'. eauto. }
    assert (∃ n : nat, ∀ y, y ∈ xs → ∀ (x' : X) (m : nat), nsteps R m y x' → m < n) as Hx'.
    - clear Hbounded'. induction xs as [|y xs IH]; first by exists 0; set_solver.
      destruct (Hx y) as [n1 Hx'']; first set_solver.
      destruct (IH) as [n2 Hxs]; first set_solver. exists (n1 `max` n2).
      intros z. rewrite elem_of_cons. intros [->|?].
      + intros ?? Hsteps. eapply Hx'' in Hsteps. lia.
      + intros x' m. intros ?. eapply Hxs in H1; last done. lia.
    - destruct Hx' as [n Hx']. exists (1 + n). intros x' m Hsteps.
      inversion Hsteps; subst; first lia. eapply Hbounded' in H1.
      eapply Hx' in H1; last done. lia.
  Qed.


  Lemma finite_nondet_ex_loop_diverge {X: Type} (R: relation X) (x: X):
    (∀ x, ∃ X: list X, ∀ x', R x x' ↔ x' ∈ X) →
    (∀ n, ∃ m, n ≤ m ∧ ∃ x', nsteps R m x x') →
    ex_loop R x.
  Proof.
    intros Hb Hsteps. eapply ex_loop_sn. destruct (classic (sn R x)) as [Hsn|]; last done.
    eapply sn_finite_nondet_bounded in Hsn; last done.
    destruct Hsn as [n Hsn]. destruct (Hsteps n) as [m [Hle [x' Hsteps']]].
    eapply Hsn in Hsteps'. lia.
  Qed.

  Lemma ex_loop_extract_finite_execution {X: Type} (R: relation X) (x: X) n:
    ex_loop R x →
    ∃ x', nsteps R n x x'.
  Proof.
    intros Hloop. induction n as [|n IH] in x, Hloop |-*.
    - exists x. constructor.
    - destruct Hloop as [x x' Hstep Hloop]. eapply IH in Hloop.
      destruct Hloop as [x'' Hsteps]. exists x''. econstructor; eauto.
  Qed.



  (* We assume a source and a target language *)
  Variable (S T: Type) (src_step: S → S → Prop) (tgt_step: T → T → Prop).
  Variable (V: Type) (val_to_src: V → S) (val_to_tgt: V → T).
  Variable (val_irred: ∀ v, ¬ ∃ t', tgt_step (val_to_tgt v) t') (val_inj: Inj eq eq val_to_tgt).

  Definition sim_finite_ref (t: T) (s: S) :=
    ∀ n t', nsteps tgt_step n t t' →
    ∃ m s', nsteps src_step m s s' ∧ n ≤ m ∧
      (∀ v, val_to_tgt v = t' → val_to_src v = s').

  Notation sim := (sim (Σ := Σ) S T src_step tgt_step V val_to_src val_to_tgt).
  Notation tpr := (tpr S T src_step tgt_step V val_to_src val_to_tgt).

  Lemma satisfiable_laterN n (P: iProp Σ): satisfiable (▷^n P)%I → satisfiable P.
  Proof. induction n; eauto using satisfiable_later. Qed.

  Lemma sim_to_sim_finite_ref t s: (⊢ sim t s) → sim_finite_ref t s.
  Proof.
    intros Hsim n t' Hsteps.
    eapply sim_valid_satisfiable in Hsim.
    eapply satisfiable_pure, (satisfiable_laterN n), satisfiable_mono; eauto.
    iIntros "Hsim". clear Hsim. iInduction Hsteps as [] "IH" forall (s).
    - rewrite sim_unfold. iDestruct "Hsim" as "[H|[H _]]".
      + iDestruct "H" as (v')  "[<- <-]". iPureIntro.
        eexists _, _. split; first constructor. split; first lia.
        intros v <-%val_inj. done.
      + iDestruct "H" as (t') "%". iPureIntro.
        eexists _, _. split; first constructor. split; first lia.
        intros ??. exfalso. naive_solver.
    - rewrite sim_unfold. iDestruct "Hsim" as "[H|[H X]]".
      + iDestruct "H" as (v')  "[<- <-]". exfalso. naive_solver.
      + iDestruct "H" as (t') "_". iDestruct ("X" with "[//]") as "X".
        iDestruct "X" as (s' Hstep) "Hsim". simpl. iNext. simpl.
        iDestruct ("IH" with "Hsim") as "Hn". iNext. simpl.
        iDestruct "Hn" as (m s'') "%". iPureIntro. destruct H0 as (?&?).
        eexists _, s''. split; first eauto using nsteps_l. split; first lia.
        naive_solver.
  Qed.

  Lemma sim_finite_ref_tpr t s:
    (∀ s, ∃ ss: list S, ∀ s', src_step s s' ↔ s' ∈ ss) →
    sim_finite_ref t s →
    tpr t s.
  Proof.
    intros Hb Href. split.
    - intros v Hsteps. eapply rtc_nsteps in Hsteps as [n Hsteps].
      eapply Href in Hsteps as (m & s'&?&?& Hv); eauto.
      specialize (Hv v eq_refl). subst. by eapply nsteps_rtc.
    - intros Hloop. eapply finite_nondet_ex_loop_diverge; first done.
      intros n. eapply (ex_loop_extract_finite_execution _ _ n) in Hloop as [x' Hsteps].
      eapply Href in Hsteps as (m & s'&?&?&?); eauto.
  Qed.

End finite_stepindex_simulation.

